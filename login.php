<?php
session_start();
if(isset($_GET['logOut']) AND $_GET['logOut']=='true'){
  session_destroy();
}
if(isset($_SESSION['loginId'])){
  header("location: index.php");
}
 ?>
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hostel Management System</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<style>
    body{position:relative;background: url('img/bg.jpg') no-repeat center center fixed;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;width:100%;height:100%;margin:0;}
    body:after{
        position:fixed;
        content:"";
        top:0;
        left:0;
        right:0;
        bottom:0;
        background:#414141;
        opacity:0.5;
        z-index:-1;
    }
</style>
<div class="loginBody">
    <div class="container">

        <form method="POST" action="loginAction.php">
          <div class="row">
              <div class="col-md-6 col-md-offset-3 col-xs-12">
                  <div class="jumbotron jumbotronLogin">
                      <h2 class="text-center"><small>Hostel management system</small></h2>
                      <div class="form-group">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-user"></span>
                              </div>
                              <input type="text" class="input form-control" name="userName" placeholder="User Name">
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-lock"></span>
                              </div>
                              <input type="password" class="input form-control" name="password" placeholder="Password">
                          </div>
                      </div>
                      <div class="form-group">
                          <button type="submit"  class="form-control btn btn-primary">Login</button>
                      </div>
                      <p class="text-center text-danger">
                          <?php
                            if(isset($_SESSION['loginFailed'])){
                                echo $_SESSION['loginFailed'];
                                unset($_SESSION['loginFailed']);
                            }
                          ?>
                      </p>
                  </div>
              </div>
          </div>
        </form>

    </div>
</div>


<script type="text/javascript" src="js/jQuery-2.1.4.min.js"></script>
<script>
  $("[type=submit]").click(function(){
  	var returnStatus=true;
    var userName=$("[name=userName]").val();
    var password=$("[name=password]").val();
    if($.trim(userName)==''){
      $("[name=userName]").addClass('input-error');
      returnStatus=false;
    }else{
      $("[name=userName]").removeClass('input-error');

    }
    if(password==''){
      $("[name=password]").addClass('input-error');
       returnStatus=false;
    }else{
      $("[name=password]").removeClass('input-error');
    }
    if (returnStatus){
    	return true;
    }else{
    	return false;
    }
  });
</script>