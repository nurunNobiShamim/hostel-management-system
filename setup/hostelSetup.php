<?php
$queryHostel=$connect->query("SELECT * FROM `hostel` ORDER BY `id` ASC");

$data='';
$sl=0;
while ($restult=$queryHostel->fetchObject()) {
	$sl++;
	$data.="
		<tr>
			<td width='8%'>{$sl}</td>
			<td>{$restult->name}</td>
			<td width='8%'>{$restult->total_floor}</td>
			<td width='5%'>
				<button type='button' class='btn btn-info'>
					<span class='glyphicon glyphicon-pencil'></span>
				</button>
			</td>
			<td width='8%'>
				<button type='button' class='btn btn-danger'>
					<span class='glyphicon glyphicon-trash'></span>
				</button>
			</td>
		</tr>

	";
}

?>
<div class="col-md-8 col-md-offset-2">
	<button class="btn btn-primary pull-right m-b15 addHostel">Add New</button>
	<table class="table table-bordered table-hover" width="100%" align="center">
		<thead>
			<tr>
				<th>S/L</th>
				<th>Hostel Name</th>
				<th>Floor</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php echo $data; ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$('.addHostel').click(function (){
			swal({
				title: 'Add New Hostel',
				text: 'Please enter your new hostel name.',
				type: 'input',
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder:'Hostel Name',
				animation: "slide-from-top"

			},function(inputValue){
				if(inputValue===false){ return false; }
				if(inputValue=== ""){
					swal.showInputError('You need to write Hostel Name!');
					return false;
				}
				swal.disableButtons();

				$.ajax({
					url: 'action/hostelSetupAction.php',
					type: 'POST',
					data: {hostelName: inputValue},
					beforeSend: function () {

					},
					success:function(data){
						swal({
							title: 'Added Successfully',
							text: '<strong>'+ data + '</strong>',
							type: 'success',
							html: true
						});
					}
				}).done(function() {
					console.log("success");
				});

			});
			return false;
		});
	});
</script>