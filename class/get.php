<?php
class get{
    public static function userType(){
        if(!isset($_SESSION['loginId'])){
            return false;
        }
        return $_SESSION['loginId']['user_type'];
    }
    public static function userId(){
        if(!isset($_SESSION['loginId'])){
            return false;
        }
        return $_SESSION['loginId']['id'];
    }
}
