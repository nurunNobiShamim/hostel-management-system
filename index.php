<?php require_once('loginValidation.php');?>
<?php require 'db.php'; ?>
<?php include('include/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
 <?php include('include/left_side.php');?>
      <div class="content-wrapper">
      	<div class="content-header container">
		<?php 
			if(isset($_GET['page'])){
				$page=$_GET['page'];
				if(isset($_GET['folder'])){
					$folder=$_GET['folder'];
				}else{
					$folder=false;
				}
				if($folder){
					include($folder.'/'.$page.'.php');
				}else{
					include($page.'.php');
				}

			}
		?>
		</div>
      </div>
  
      <!-- Content Wrapper. Contains page content -->
      
  <?php include('include/footer.php');?>