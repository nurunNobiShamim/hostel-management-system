-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2015 at 07:12 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hostel`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_cost`
--

CREATE TABLE IF NOT EXISTS `daily_cost` (
  `id` int(11) NOT NULL,
  `total_cost` int(10) NOT NULL,
  `purchased` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `purchased_date` date NOT NULL,
  `uploader` int(11) NOT NULL,
  `approve` bit(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(2) NOT NULL,
  `dept_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE IF NOT EXISTS `employee_info` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `emp_name` varchar(30) NOT NULL,
  `birthDay` varchar(10) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `emp_type` varchar(30) NOT NULL,
  `qualification` varchar(50) NOT NULL,
  `city` varchar(20) NOT NULL,
  `division` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `salary` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fixed_meal`
--

CREATE TABLE IF NOT EXISTS `fixed_meal` (
  `id` int(11) NOT NULL,
  `maxi` int(2) NOT NULL,
  `mini` int(2) NOT NULL,
  `fixed` int(2) NOT NULL,
  `uploader` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guest_meal`
--

CREATE TABLE IF NOT EXISTS `guest_meal` (
  `id` int(3) NOT NULL,
  `meal_rate` int(3) NOT NULL,
  `uploader` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hostel`
--

CREATE TABLE IF NOT EXISTS `hostel` (
  `id` int(5) NOT NULL,
  `name` varchar(70) NOT NULL,
  `total_floor` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hostel`
--

INSERT INTO `hostel` (`id`, `name`, `total_floor`) VALUES
(1, 'fci boys hostel', 5),
(2, 'fci girls hostel', 5);

-- --------------------------------------------------------

--
-- Table structure for table `hostel_student`
--

CREATE TABLE IF NOT EXISTS `hostel_student` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `resident` bit(1) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(12) NOT NULL,
  `uploader` int(11) NOT NULL,
  `approve` int(11) NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `institute_info`
--

CREATE TABLE IF NOT EXISTS `institute_info` (
  `id` int(1) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` int(8) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `established` varchar(10) NOT NULL,
  `address` varchar(200) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `website` varchar(60) NOT NULL,
  `facebook` varchar(70) NOT NULL,
  `twitter` varchar(70) NOT NULL,
  `googleplus` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user`, `password`, `user_type`, `active`) VALUES
(2, 'admin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'admin', b'0'),
(3, 'admin1', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'admin', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `money_collection`
--

CREATE TABLE IF NOT EXISTS `money_collection` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `ammount` int(11) NOT NULL,
  `total_ammount` int(11) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `old_student`
--

CREATE TABLE IF NOT EXISTS `old_student` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `uploader` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `parmanent_address`
--

CREATE TABLE IF NOT EXISTS `parmanent_address` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `village` varchar(30) NOT NULL,
  `house_name` varchar(50) NOT NULL,
  `holding_no` varchar(10) NOT NULL,
  `post_office` varchar(15) NOT NULL,
  `upzilla` varchar(15) NOT NULL,
  `district` varchar(15) NOT NULL,
  `division` varchar(15) NOT NULL,
  `postal_code` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `present_address`
--

CREATE TABLE IF NOT EXISTS `present_address` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `village` varchar(35) NOT NULL,
  `house_name` varchar(35) NOT NULL,
  `holding_no` int(10) NOT NULL,
  `post_office` varchar(15) NOT NULL,
  `upzilla` varchar(15) NOT NULL,
  `district` varchar(15) NOT NULL,
  `division` varchar(15) NOT NULL,
  `postal_code` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL,
  `hostel_id` int(5) NOT NULL,
  `number_of_floor` int(2) NOT NULL,
  `number_of_room` int(5) NOT NULL,
  `availity` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `hostel_id`, `number_of_floor`, `number_of_room`, `availity`) VALUES
(1, 1, 5, 505, 5);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `ammount` int(10) NOT NULL,
  `uploader` int(11) NOT NULL,
  `approve` int(11) NOT NULL,
  `details` varchar(200) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE IF NOT EXISTS `shift` (
  `id` int(11) NOT NULL,
  `shift_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_basic_info`
--

CREATE TABLE IF NOT EXISTS `student_basic_info` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `father's_name` varchar(30) NOT NULL,
  `mother's_name` varchar(30) NOT NULL,
  `department_id` int(2) NOT NULL,
  `rool` int(10) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `date_of_birth` date NOT NULL,
  `religion` varchar(15) NOT NULL,
  `marital_status` varchar(8) NOT NULL,
  `blood_group` varchar(4) NOT NULL,
  `session` varchar(10) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `image` varchar(45) NOT NULL,
  `uploader` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temporary_meal`
--

CREATE TABLE IF NOT EXISTS `temporary_meal` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `meals` int(2) NOT NULL,
  `guest_meals` int(2) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `total_meals`
--

CREATE TABLE IF NOT EXISTS `total_meals` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `total_guest_meal` int(5) NOT NULL,
  `total_meal` int(11) NOT NULL,
  `total_taka` int(11) NOT NULL,
  `uploader` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `id` int(4) NOT NULL,
  `year_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='different name is class';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily_cost`
--
ALTER TABLE `daily_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixed_meal`
--
ALTER TABLE `fixed_meal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest_meal`
--
ALTER TABLE `guest_meal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel`
--
ALTER TABLE `hostel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_student`
--
ALTER TABLE `hostel_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `student_id_2` (`student_id`),
  ADD KEY `room_id_2` (`room_id`);

--
-- Indexes for table `institute_info`
--
ALTER TABLE `institute_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `money_collection`
--
ALTER TABLE `money_collection`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `old_student`
--
ALTER TABLE `old_student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `room_id_2` (`room_id`);

--
-- Indexes for table `parmanent_address`
--
ALTER TABLE `parmanent_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `present_address`
--
ALTER TABLE `present_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hostel_id` (`hostel_id`),
  ADD KEY `hostel_id_2` (`hostel_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_basic_info`
--
ALTER TABLE `student_basic_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_id` (`login_id`);

--
-- Indexes for table `temporary_meal`
--
ALTER TABLE `temporary_meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `total_meals`
--
ALTER TABLE `total_meals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `year`
--
ALTER TABLE `year`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily_cost`
--
ALTER TABLE `daily_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fixed_meal`
--
ALTER TABLE `fixed_meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guest_meal`
--
ALTER TABLE `guest_meal`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hostel`
--
ALTER TABLE `hostel`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hostel_student`
--
ALTER TABLE `hostel_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institute_info`
--
ALTER TABLE `institute_info`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `money_collection`
--
ALTER TABLE `money_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `old_student`
--
ALTER TABLE `old_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parmanent_address`
--
ALTER TABLE `parmanent_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `present_address`
--
ALTER TABLE `present_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_basic_info`
--
ALTER TABLE `student_basic_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temporary_meal`
--
ALTER TABLE `temporary_meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `total_meals`
--
ALTER TABLE `total_meals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `year`
--
ALTER TABLE `year`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `hostel_student`
--
ALTER TABLE `hostel_student`
  ADD CONSTRAINT `hostel_student_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hostel_student_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `money_collection`
--
ALTER TABLE `money_collection`
  ADD CONSTRAINT `money_collection_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `old_student`
--
ALTER TABLE `old_student`
  ADD CONSTRAINT `old_student_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parmanent_address`
--
ALTER TABLE `parmanent_address`
  ADD CONSTRAINT `parmanent_address_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `present_address`
--
ALTER TABLE `present_address`
  ADD CONSTRAINT `present_address_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`hostel_id`) REFERENCES `hostel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `salary_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_basic_info`
--
ALTER TABLE `student_basic_info`
  ADD CONSTRAINT `student_basic_info_ibfk_1` FOREIGN KEY (`login_id`) REFERENCES `login` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `temporary_meal`
--
ALTER TABLE `temporary_meal`
  ADD CONSTRAINT `temporary_meal_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `total_meals`
--
ALTER TABLE `total_meals`
  ADD CONSTRAINT `total_meals_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_basic_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
