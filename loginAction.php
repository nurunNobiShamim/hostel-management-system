<?php
session_start();
require_once 'db.php';
$user='';
$password='';
$user=trim($_POST['userName']);
$password=$_POST['password'];
if(!empty($user) AND !empty($password)){
	$passwordSha=sha1($password);
	$loginPre=$connect->prepare("SELECT `id`,`user_type`,`active` FROM `login` WHERE `user`=? AND `password`=?");
	$loginPre->bindParam(1,$user);
	$loginPre->bindParam(2,$passwordSha);
	$loginPre->execute();

	$loginRows=$loginPre->fetchAll(PDO::FETCH_ASSOC);
	$total=count($loginRows);
	
	if($total==1){
		
		if($loginRows[0]['active']==0){
			$_SESSION['loginFailed']='your account is temporary blocked.';
			header("location:login.php");
		}else{
			 $_SESSION['loginId']=$loginRows[0];
			header("Location: index.php");
		}
	}else{
		$_SESSION['loginFailed']='User or password does not matched.';
		header("Location: login.php");
	}
}
?>